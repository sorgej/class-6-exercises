/**
 * Creates a new Car object
 * @constructor
 * @param {String} model 
 */
const Car = function(model) {
    this.currentSpeed = 0;
    this.model = model;
};

Car.prototype.accelerate = function () {
    this.currentSpeed++;
};

Car.prototype.brake = function() {
    this.currentSpeed--;
};

Car.prototype.toString = function() {
    return `This ${this.model} car has a speed of ${this.currentSpeed}`;
};

