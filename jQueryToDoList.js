$(document).ready(function() {
    // If an li element is clicked, toggle the class "done" on the <li>
    let $li = $('li')  // create jQuery object of li elements
    // console.log($li);

    // Add event listener to ALL li elements
    const toggleToDo = function(e) {
        let $this = $(this); 
        $(this).parent().toggleClass('done');
    }


    // If a delete link is clicked, delete the li element / remove from the DOM
    const deleteToDo = function(e) {
        let $this = $(this); 
        $this.parent()[0].style.opacity = '0';
        setTimeout(function(){$this.parent().remove();}, 500);
        //$(this).parent().remove(); // add to parent class
    }


    // If a "Move to..."" link is clicked, it should move the item to the correct
    // list.  Should also update the working (i.e. from Move to Later to Move to Today)
    // and should update the class for that link.
    // Should *NOT* change the done class on the <li>
    const moveToDo = function(e) {
        let $this = $(this); 
        let $thisParent = $(this).parent();
        $aEl = $this[0];
        //console.log($aEl);
        

        if ($this.hasClass('toLater')) {
            $this.removeClass('toLater');
            $this.addClass('toToday');
            $this.text('Move to Today');
            // console.log($('.later-list'))
            let removedDeletedLi = $thisParent.remove();
            $('.later-list').append($thisParent);

        } else if ($this.hasClass('toToday')) {
            $this.removeClass('toToday');
            $this.addClass('toLater');
            $this.text('Move to Later');
            let removedDeletedLi = $thisParent.remove();
            $('.today-list').append($thisParent);
        };
        // make sure new li sub-elements have click events
        $this.prev().on('click', toggleToDo);
        $this.next().on('click', deleteToDo);
        $this.on('click', moveToDo);
    };


    // If an 'Add' link is clicked, adds the item as a new list item in correct list
    // addListItem function has been started to help you get going!  
    // Make sure to add an event listener to your new <li> (if needed)
    const addListItem = function(e) {
        e.preventDefault();
        const text = $(this).parent().find('input').val();
        
        // Create li plus subelements/classes
        let $newLi = $('<li>');
        let $newSpan = $('<span>');
        $newSpan.text(text);
        let $newAMove = $('<a>');
        $newAMove.addClass('move');

        let $newADel = $('<a>');
        $newADel.text('Delete');
        $newADel.addClass('delete');

        // Put elements together
        $newLi.append($newSpan);
        $newLi.append($newAMove);
        $newLi.append($newADel);

        // put together today / later elements and append to ul 
        if ($(this).parent ().parent().hasClass('today')) {
            $newAMove.text('Move to Later');
            $newAMove.addClass('toLater');
            $('.today-list').append($newLi);
        } else if ($(this).parent().parent().hasClass('later')) {
            $newAMove.text('Move to Today');
            $newAMove.addClass('toToday');
            $('.later-list').append($newLi);
        };

        // TODO how do I clear input text?
        //console.log($(this).prev().text(''))
        // Add this as a listener to a elemnts
        $newSpan.on('click', toggleToDo);
        $newADel.on('click', deleteToDo);
        $newAMove.on('click', moveToDo);
    };
    // here are event listeners
    $('span').on('click', toggleToDo);
    $('.delete').on('click', deleteToDo);
    $('.move').on('click', moveToDo);
    $('.add-item').on('click', addListItem);
});