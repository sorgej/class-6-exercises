$(document).ready(function() {
    // Create a new <a> element containing the text "Buy Now!" 
    // with an id of "cta" after the last <p>
    let $newA = $('<a>');
    $newA.text('Buy Now!');
    $newA.attr('id', 'cta');
    //console.log($newA);
    $('main').append($newA);

    // Access (read) the data-color attribute of the <img>,
    // log to the console
    const $img = $('img');
    console.log($img[0].dataset.color);

    // Update the third <li> item ("Turbocharged",
    // set the class name to "highlight"
    const $li = $('li');
    const $li3 = $li[2];
    $li3.setAttribute('Class','highlight');

    // Remove (delete) the last paragraph
    // (starts with "Available for purchase now…")
    const $p = $('p');
    //console.log($p[0]);
    let removedPEl = $p[0].remove();
})