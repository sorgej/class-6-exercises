// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click


let tdEls =
document.getElementsByTagName('td');
for (let i = 0; i < tdEls.length; i++) {
    tdEls[i].addEventListener('click', function(e){
        let tdEl = e.target; // = this;
        this.innerHTML = `${e.clientX}, ${e.clientY}`;
    });
}